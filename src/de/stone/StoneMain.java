package de.stone;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.lang3.ArrayUtils;

import de.stone.core.Globals;
import de.stone.core.interpreter.Interpreter;
import de.stone.core.preprocessor.Preprocessor;

public class StoneMain {

	public static void main(String[] args) {

		Interpreter inter = Globals.inter;
		File f;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-f")) {
				f = new File(args[i + 1]);
				passFileToInter(f);
				i = i + 1;
			}
			if (args[i].equals("-h")) {
				System.out.println("Synopsis: stone [-f filename.stone] [options]");
				System.out.println();
				System.out.println("Options:");
				System.out.println("-h		Show this help menu");
			}

		}

	}

	private static void passFileToInter(File f) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			String[] lines = null;

			String line;

			while ((line = br.readLine()) != null) {
				lines = ArrayUtils.add(lines, line);
			}

			Preprocessor.processAndInterpret(lines, f.getAbsolutePath());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
