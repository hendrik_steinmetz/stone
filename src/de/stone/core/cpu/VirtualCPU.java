package de.stone.core.cpu;

import java.util.HashMap;

public class VirtualCPU {

	HashMap<String, Double> regs = new HashMap<>();

	public VirtualCPU() {
		regs.put("ax", 0.0);
		regs.put("cx", 0.0);
		regs.put("dx", 0.0);
		regs.put("res", 0.0);
	}

	public void mov(String srcReg, String destReg) {
		Double srcVal = regs.get(srcReg);
		regs.replace(destReg, srcVal);
		regs.replace(srcReg, 0.0);
	}

	public void set(String reg, Double value) {
		regs.replace(reg, value);
	}

	public Double get(String reg) {
		return regs.get(reg);
	}

}
