package de.stone.core;

import de.stone.core.cpu.VirtualCPU;
import de.stone.core.interpreter.Interpreter;
import de.stone.core.stacks.ArrayStack;
import de.stone.core.stacks.FunctionStack;
import de.stone.core.stacks.VariableStack;

public class Globals {

	public static ArrayStack arrStack = new ArrayStack();
	public static VariableStack varStack = new VariableStack();
	public static VirtualCPU cpu = new VirtualCPU();
	public static FunctionStack funcStack = new FunctionStack();
	public static Interpreter inter = new Interpreter();

}
