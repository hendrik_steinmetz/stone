package de.stone.core.interpreter;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;

import de.stone.core.FunctionInvoker;
import de.stone.core.Globals;
import de.stone.core.exceptions.FunctionInFunctionException;
import de.stone.core.exceptions.MisplacedEndInstructionException;

public class Interpreter {

	boolean inIfBody = false;
	boolean inForBody = false;
	int forFrom, forTo, forCounter;
	boolean ifCondIsMet = false;
	boolean inFunctionBody = false;
	boolean returned = false;
	String functionName = "";
	Pattern forPattern = Pattern.compile("for [0-9]+ to [0-9]+");
	String[] forLines = ArrayUtils.EMPTY_STRING_ARRAY;

	public Interpreter() {
		Globals.varStack.push("lastGet", 0);
	}

	public void interpret(String line) {
		line = line.trim();
		if (line.startsWith("//"))
			return;
		String[] tokens = line.split(" ");
		ArrayList<String> params = new ArrayList<>();

		for (int i = 1; i < tokens.length; i++) {
			params.add(tokens[i]);
		}

		if (line.equals("endfunc")) {
			if (this.inFunctionBody) {
				this.inFunctionBody = false;
				this.functionName = "";
			} else {
				new MisplacedEndInstructionException("Misplaced endfunc statement!");
			}
			return;
		}

		if (this.inForBody) {
			for (; forFrom < forTo; forFrom++) {
				this.inForBody = false;
				interpret(line);
			}
		}

		if (line.equals("endfor")) {
			if (this.inForBody && forCounter == forTo) {
				this.inForBody = false;
				this.forCounter = 0;
				this.forFrom = 0;
				this.forTo = 0;
				return;
			} else {
				new MisplacedEndInstructionException("Misplaced endfor statement!");
			}
		}

		if (this.inFunctionBody) {
			Globals.funcStack.addLine(this.functionName, line);
			return;
		}

		if (line.startsWith("func")) {
			this.inFunctionBody = true;
			this.functionName = tokens[1];
			Globals.funcStack.add(this.functionName);
			return;
		}

		if (forPattern.matcher(line).matches()) {
			this.inForBody = true;
			this.forFrom = Integer.valueOf(tokens[1]);
			this.forTo = Integer.valueOf(tokens[3]);
			this.forCounter = forFrom;
			return;
		}

		if (line.startsWith("$")) {
			if (tokens[1].equals("=")) {
				setVariable(line, tokens);
				Globals.varStack.set(tokens[0].substring(1), removeQuotes(evalVar(tokens[2])));
			}
			return;
		}

		if (tokens[0].equals("func") && inFunctionBody) {
			new FunctionInFunctionException(
					"Tried to declare function '" + tokens[1] + "' in function '" + functionName + "'");
			return;
		}

		if (line.equals("endif")) {
			this.ifCondIsMet = false;
			this.inIfBody = false;
			return;
		}

		if ((this.inIfBody && !this.ifCondIsMet) || (this.inIfBody && !this.ifCondIsMet && this.inFunctionBody))
			return;

		if (tokens[0].equals("if")) {
			if (inFunctionBody) {
				Globals.funcStack.addLine(functionName, line);
				return;
			}
			this.inIfBody = true;
			if (interpretIf(line)) {
				this.ifCondIsMet = true;
				return;
			} else {
				this.ifCondIsMet = false;
				return;
			}
		}

		if (tokens[0].equals("call")) {
			tokens = ArrayUtils.remove(tokens, 0);
			String name = tokens[0];
			tokens = ArrayUtils.remove(tokens, 0);
			Globals.funcStack.call(name, tokens);
			return;
		}

		if (line.startsWith("println")) {
			if (line.equals("println")) {
				System.out.println();
			}
			if (tokens[1].startsWith("\"")) {
				String value = line.substring(line.indexOf("\""), line.lastIndexOf("\""));
				value = value.replaceAll("\"", "");
				FunctionInvoker.invoke("println", value);
				return;
			}
		}

		if (line.startsWith("print")) {
			if (tokens[1].startsWith("\"")) {
				String value = line.substring(line.indexOf("\""), line.lastIndexOf("\""));
				value = value.replaceAll("\"", "");
				FunctionInvoker.invoke("print", value);
				return;
			}
		}

		if (line.startsWith("var")) {
			interpretVar(line, tokens);

		}

		FunctionInvoker.invoke(tokens[0], params.toArray(tokens));
		return;
	}

	private void setVariable(String line, String[] tokens) {
		Pattern arrayPattern = Pattern.compile("\\$.+\\[[0-9]\\] = .+");
		if (line.startsWith("$")) {
			if (arrayPattern.matcher(line).matches()) {
				String name = tokens[0].split("\\[")[0].substring(1);
				int index = Integer.valueOf(evalVar((tokens[0].replaceAll(".+\\[", "").replaceAll("\\]", ""))));
				String value = evalVar(tokens[2]);
				Object actualValue = value;
				String typeOfArray = Globals.arrStack.getByName(name).getType();
				switch (typeOfArray) {
				case "int":
					actualValue = new Integer(value);
					break;
				case "String":
					actualValue = new String(value);
					break;
				case "double":
					actualValue = new Double(value);
					break;
				}
				Globals.arrStack.getByName(name).setValueAtIndex(index, actualValue);
				return;
			} else {
				Globals.varStack.set(tokens[0].substring(1), tokens[2]);
				return;
			}
		}
	}

	private String removeQuotes(String toQ) {
		toQ = toQ.replaceAll("\"", "");
		return toQ;
	}

	private boolean interpretIf(String line) {
		String[] tokens = line.split(" ");

		String obj1 = removeQuotes(evalVar(tokens[1]));
		String op = tokens[2];
		String obj2 = removeQuotes(evalVar(tokens[3]));

		switch (op) {
		case "eq":
			return obj1.equals(obj2);
		case "sml":
			if (Double.valueOf(obj1) < Double.valueOf(obj2)) {
				return true;
			} else {
				return false;
			}
		case "grt":
			if (Double.valueOf(obj1) > Double.valueOf(obj2)) {
				return true;
			} else {
				return false;
			}
		case "smleq":
			if (Double.valueOf(obj1) <= Double.valueOf(obj2)) {
				return true;
			} else {
				return false;
			}
		case "grteq":
			if (Double.valueOf(obj1) >= Double.valueOf(obj2)) {
				return true;
			} else {
				return false;
			}
		case "neq":
			if (!obj1.equals(obj2)) {
				return true;
			} else {
				return false;
			}
		default:
			ifCondIsMet = false;
			return false;
		}

	}

	public String evalVar(String str) {
		Pattern arrayPattern = Pattern.compile("\\$.+\\[.+]");
		if (str.startsWith("%")) {
			String[] tokens = str.split(" ");
			tokens = ArrayUtils.remove(tokens, 0);
			for (String s : tokens)
				System.out.println(s);
			String retnVal = Globals.funcStack.call(str.substring(1), tokens);
			return retnVal;
		} else if (!str.startsWith("$")) {
			return str;
		} else if (arrayPattern.matcher(str).matches()) {
			String var = str.substring(1, str.indexOf("["));
			int index = Integer.valueOf(evalVar(str.substring(str.indexOf("[") + 1, str.lastIndexOf("]"))));
			return Globals.arrStack.getByName(var).getValueAtIndex(index).toString().replaceAll("\"", "");
		} else if (str.startsWith("$")) {
			String var = str.substring(1, str.length());
			return Globals.varStack.getByName(var).toString();
		} else {
			String var = str.substring(1, str.length());
			return Globals.varStack.getByName(var).toString();
		}
	}

	public void interpret(String[] lines) {
		for (String line : lines)
			interpret(line);
	}

	public void interpretVar(String line, String[] tokens) {
		Pattern listPattern = Pattern.compile("var .+ = array\\([0-9]\\) :: .+");
		Pattern functionReturn = Pattern.compile("var .+ = %[a-zA-Z0-9]+ ?.+");
		Pattern valueFromArrayPattern = Pattern.compile("var .+ = \\$.+\\[.+\\]");
		boolean valueFromArray = valueFromArrayPattern.matcher(line).matches();
		boolean functionReturnValue = functionReturn.matcher(line).matches();
		Matcher listMatcher = listPattern.matcher(line);
		boolean array = listMatcher.matches();
		Integer arraySize = 0;

		if (valueFromArray) {
			String arrName = tokens[3].substring(1, tokens[3].indexOf("["));
			int index = Integer
					.valueOf(evalVar(tokens[3].substring(tokens[3].indexOf("[") + 1, tokens[3].indexOf("]"))));
			Globals.varStack.push(tokens[1], Globals.arrStack.getByName(arrName).getValueAtIndex(index));
			return;
		}

		if (array) {
			tokens[3] = tokens[3].replaceAll("array\\(", "");
			tokens[3] = tokens[3].replaceAll("\\)", "");
			arraySize = Integer.valueOf(tokens[3]);
		}

		if (functionReturnValue) {
			String[] params = new String[] {};
			for (int i = 4; i < tokens.length; i++) {
				params = ArrayUtils.add(params, tokens[i]);
			}
			tokens[3] = Globals.funcStack.call(tokens[3].substring(1), params);
			return;
		}

		if (array) {
			switch (tokens[5]) {
			case "int":
				Globals.arrStack.add("int", tokens[1], arraySize);
				break;
			case "String":
				Globals.arrStack.add("String", tokens[1], arraySize);
				break;
			case "double":
				Globals.arrStack.add("double", tokens[1], arraySize);
				break;
			}
		}
		if (tokens[3].equals("null")) {
			Globals.varStack.push(tokens[1], null);
			return;
		}
		if (tokens.length == 5) {
			if (tokens[4].equals("::")) {
				switch (tokens[5]) {
				case "int":
					Globals.varStack.push(tokens[1], Integer.valueOf(tokens[3]));
					break;
				case "double":
					Globals.varStack.push(tokens[1], Double.valueOf(tokens[3]));
					break;
				case "long":
					Globals.varStack.push(tokens[1], Long.valueOf(tokens[3]));
					break;
				case "short":
					Globals.varStack.push(tokens[1], Short.valueOf(tokens[3]));
					break;
				}
				return;
			}
		}
		if (tokens[3].startsWith("\"")) {
			String name = tokens[1];
			String value = line.substring(line.indexOf("\""), line.lastIndexOf("\""));
			value = value.replaceAll("\"", "");
			Globals.varStack.push(name, value);
			return;
		} else {
			Globals.varStack.push(tokens[1], evalVar(tokens[3]));
			return;
		}
	}

	public String interpretFunction(String line, String[] parameters, String functionName) {
		line = line.trim();

		if (line.startsWith("//"))
			return "";

		String[] tokens = line.split(" ");
		ArrayList<String> params = new ArrayList<>();

		for (int i = 1; i < tokens.length; i++) {
			params.add(tokens[i]);
		}

		if (parameters.length > 0) {
			// Replace all parameter expressions with the actual parameter value
			for (int i = 0; i < tokens.length; i++) {
				if (tokens[i].startsWith("&")) {
					tokens[i] = parameters[Integer.valueOf(tokens[i].substring(1)) - 1];
				}
			}
		}

		if (tokens[0].equals("return")) {
			returned = true;
			return tokens[1].startsWith("$") ? evalVar(tokens[1]) : tokens[1];
		}

		StringBuilder sb = new StringBuilder();
		for (String s : tokens) {
			sb.append(s + " ");
		}

		interpret(sb.toString());
		return "";
	}

}
