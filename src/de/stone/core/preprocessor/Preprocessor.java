package de.stone.core.preprocessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import de.stone.core.Globals;
import de.stone.util.Location;

public class Preprocessor {

	public static String lib_path = Location.class.getProtectionDomain().getCodeSource().getLocation().toString();

	public static void processAndInterpret(String[] lines, String dir) {

		lines = ArrayUtils.add(lines, "#include std");

		lib_path = lib_path.substring("file:/".length(), lib_path.length());
		lib_path = StringUtils.replace(lib_path, "/", "\\\\");
		lib_path += "\\..";

		for (int i = 0; i < lines.length; i++) {
			String[] tokens = lines[i].split(" ");

			if (tokens[0].equals("#define")) {
				if (tokens[1].equals("private")) {
					// Do nothing
				} else {
					for (int j = i + 1; j < lines.length; j++) {
						lines[j] = lines[j].replaceAll(tokens[1], tokens[2]);
					}
				}
			} else if (tokens[0].equals("#include")) {
				String filename = tokens[1];
				File toInclude;
				if (filename.endsWith(".stone")) {
					toInclude = new File(dir + "\\.." + filename);
				} else {
					toInclude = new File(lib_path + "\\" + filename + ".stone");
				}
				lines = ArrayUtils.remove(lines, i);
				String[] externalLines = ArrayUtils.EMPTY_STRING_ARRAY;

				try {
					BufferedReader br = new BufferedReader(new FileReader(toInclude));

					String externalLine = "";

					while ((externalLine = br.readLine()) != null) {
						externalLines = ArrayUtils.add(externalLines, externalLine);
					}

					if (ArrayUtils.contains(externalLines, "#define private")) {
						System.out.println();
						System.out
								.println("[WARNING] Include file \'" + toInclude.getName() + "\' cannot be included!");
						System.out.println("[WARNING] Skipping.");
						continue;
					}

					String[] newLines = externalLines;
					newLines = ArrayUtils.addAll(newLines, lines);
					lines = newLines;
				} catch (IOException e) {
					System.out.println();
					System.out.println("[ERROR] File not found: " + toInclude.getAbsolutePath() + ".");
					System.out.println("[ERROR] Aborting.");
					System.out.println();
					System.exit(0);
				}

			} else if (tokens[0].startsWith("#")) {
				System.out.println(
						"[WARNING] Unknown Preprocessor directive: '" + tokens[0] + "' on line " + (i + 1) + ".");
				System.out.println("[WARNING] This could prevent the program from running correctly.");
			}
		}

		Globals.inter.interpret(lines);
	}

}
