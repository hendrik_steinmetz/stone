package de.stone.core.lang;

import org.apache.commons.lang3.ArrayUtils;

import de.stone.core.Globals;

public class Function {

	String name = "";
	String[] body = ArrayUtils.EMPTY_STRING_ARRAY;
	String return_value = "";

	public String run(String[] params) {
		String returnStr = null;
		for (String line : body) {
			returnStr = Globals.inter.interpretFunction(line, params, name);
			if (!returnStr.equals(""))
				return returnStr;
		}

		return returnStr;
	}

	public Function(String name, String[] body, String return_value) {
		super();
		this.name = name;
		this.body = body;
		this.return_value = return_value;
	}

	public Function(String name) {
		this.name = name;
	}

	public void addLine(String line) {
		this.body = ArrayUtils.add(body, line);
	}

	public String getName() {
		return name;
	}

	public String[] getBody() {
		return body;
	}

	public String getReturnValue() {
		return return_value;
	}

	public void setReturnValue(String return_value) {
		this.return_value = return_value;
	}

}
