package de.stone.core.lang;

import java.lang.reflect.Array;

public class StoneArray {

	public Object[] array = null;
	String type;

	public StoneArray(String type, int size) {
		try {
			this.type = type;
			switch (type) {
			case "int":
				array = new Integer[size];
				for (int i = 0; i < size; i++) {
					array[i] = new Integer(0);
				}
				break;
			case "String":
				array = new String[size];
				for (int i = 0; i < size; i++) {
					array[i] = new String();
				}
				break;
			case "double":
				array = new Double[size];
				for (int i = 0; i < size; i++) {
					array[i] = new Double(0);
				}
				break;
			}
		} catch (NegativeArraySizeException e) {
			e.printStackTrace();
		}
	}

	public void setValueAtIndex(int index, Object val) {
		Array.set(array, index, val);
	}

	public Object getValueAtIndex(int index) {
		return Array.get(array, index);
	}

	public String getType() {
		return type;
	}

}
