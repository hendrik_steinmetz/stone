package de.stone.core.lang;

public class FunctionMap {

	// Print a line
	public final static String PRINTLN = "println";
	// Print something without newline
	public final static String PRINT = "print";
	// Read input from user into variable
	public final static String READ = "read";
	// Add two values
	public final static String ADD = "add";
	// Subtract two values
	public final static String SUB = "sub";
	// Multiply two values
	public final static String MULT = "mult";
	// Divide two values
	public final static String DIV = "div";
	// Mod two values
	public final static String MOD = "mod";
	// Move one register into another
	public final static String MOV = "mov";
	// Set a specific register in the virtual cpu to the specified value
	public final static String SET = "set";
	// Get the value of a specific register in the cpu and save it in the 'res'
	// system variable
	public final static String GET = "get";
	// Exit
	public final static String EXIT = "exit";
	// Concatenate two strings
	public final static String CONC = "conc";

}
