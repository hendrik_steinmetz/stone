package de.stone.core.exceptions;

public class MisplacedEndInstructionException extends Exception {

	public MisplacedEndInstructionException(String msg) {
		super(msg);
	}

	public MisplacedEndInstructionException() {
		super();
	}

}
