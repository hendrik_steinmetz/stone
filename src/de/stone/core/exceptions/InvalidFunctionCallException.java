package de.stone.core.exceptions;

public class InvalidFunctionCallException extends Exception {

	public InvalidFunctionCallException() {
		super();
	}

	public InvalidFunctionCallException(String msg) {
		super(msg);
	}

}
