package de.stone.core.exceptions;

public class FunctionInFunctionException extends Exception {

	public FunctionInFunctionException(String msg) {
		super(msg);
	}

	public FunctionInFunctionException() {
		super();
	}

}
