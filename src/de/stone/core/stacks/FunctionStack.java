package de.stone.core.stacks;

import java.util.ArrayList;

import de.stone.core.Globals;
import de.stone.core.lang.Function;

public class FunctionStack {

	private ArrayList<Function> stack = new ArrayList<>();

	public String call(String name, String[] params) {
		for (int i = 0; i < params.length; i++) {
			params[i] = Globals.inter.evalVar(params[i]);
		}

		String retn = "";

		for (Function f : stack) {
			if (f.getName().equals(name)) {
				retn = f.run(params);
				break;
			}
		}
		return retn;
	}

	public void add(String name) {
		stack.add(new Function(name));
	}

	public void addLine(String name, String line) {
		for (Function f : stack) {
			if (f.getName().equals(name)) {
				f.addLine(line);
				break;
			}
		}
		return;
	}

	private static String evalParam(String param) {
		if (param.startsWith("$")) {
			String var = param.substring(1, param.length());
			return Globals.varStack.getByName(var).toString();
		} else {
			return param;
		}
	}

}
