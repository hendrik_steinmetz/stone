package de.stone.core.stacks;

import java.util.HashMap;

import de.stone.core.lang.StoneArray;

public class ArrayStack {

	HashMap<String, StoneArray> arrays = new HashMap<>();

	public void add(String type, String name, int size) {
		arrays.put(name, new StoneArray(type, size));
	}

	public StoneArray getByName(String name) {
		return arrays.get(name);
	}

}
