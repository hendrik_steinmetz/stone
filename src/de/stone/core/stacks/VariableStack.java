package de.stone.core.stacks;

import java.util.HashMap;

public class VariableStack {

	HashMap<String, Object> stack = new HashMap<>();

	public void push(String name, Object value) {
		stack.put(name, value);
	}

	public Object getByName(String name) {
		return stack.get(name);
	}

	public void set(String name, Object value) {
		stack.replace(name, value);
	}

	public Object getType(String name) {
		return stack.get(name).getClass().getName();
	}

}
