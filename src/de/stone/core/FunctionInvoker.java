package de.stone.core;

import java.util.Scanner;

import de.stone.core.lang.FunctionMap;

public class FunctionInvoker {

	private static String evalParam(String param) {
		return Globals.inter.evalVar(param);
	}

	public static void invoke(String name, String... params) {

		switch (name) {
		case FunctionMap.PRINTLN:
			params[0] = params[0].replaceAll("\"", "");
			System.out.println(Globals.inter.evalVar(params[0]));
			break;
		case FunctionMap.PRINT:
			System.out.print(evalParam(params[0]));
			break;
		case FunctionMap.READ:
			Scanner s = new Scanner(System.in);
			Globals.varStack.set(params[0].substring(1, params[0].length()), s.nextLine());
			break;
		case FunctionMap.ADD:
			Globals.cpu.set("res", Double.valueOf(evalParam(params[0])) + Double.valueOf(evalParam(params[1])));
			break;
		case FunctionMap.SUB:
			Globals.cpu.set("res", Double.valueOf(evalParam(params[0])) - Double.valueOf(evalParam(params[1])));
			break;
		case FunctionMap.MULT:
			Globals.cpu.set("res", Double.valueOf(evalParam(params[0])) * Double.valueOf(evalParam(params[1])));
			break;
		case FunctionMap.DIV:
			Globals.cpu.set("res", Double.valueOf(evalParam(params[0])) / Double.valueOf(evalParam(params[1])));
			break;
		case FunctionMap.MOV:
			Globals.cpu.mov(params[0], params[1]);
			break;
		case FunctionMap.MOD:
			Globals.cpu.set("res", Double.valueOf(evalParam(params[0])) % Double.valueOf(evalParam(params[1])));
			break;
		case FunctionMap.SET:
			Globals.cpu.set(evalParam(params[0]), new Double(evalParam(params[1])));
			break;
		case FunctionMap.GET:
			Globals.varStack.set("lastGet", Globals.cpu.get(params[0]));
			break;
		case FunctionMap.CONC:

			String string1 = Globals.inter.evalVar(params[0]);
			String string2 = Globals.inter.evalVar(params[1]);

			String targetVariable = params[2].substring(1);

			Globals.varStack.set(targetVariable, string1.concat(string2));
			break;
		case FunctionMap.EXIT:
			System.exit(Integer.valueOf(params[0]));
			break;
		}

	}

}
